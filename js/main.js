const $ = (_querySelector) => { return document.querySelector(_querySelector); }

document.addEventListener('scroll', function(e) {
  console.log('[DEBUG]', 'Scroll position:', window.scrollY);

  if(window.scrollY <= 30) {
    $('.navBar').setAttribute('class', 'navBar');
  } else {
    $('.navBar').setAttribute('class', 'navBar navBarScrolled');
  }
});

const getContributors = () => {
  fetch('https://gitlab.com/api/v4/projects/32495057/repository/contributors')
    .then(response => response.json())
    .then(data => {
      data.forEach(user => {
        fetch('https://gitlab.com/api/v4/avatar?email=' + user.email + '&size=1024')
          .then(response => response.json())
          .then(picture => {
            const avatar = picture.avatar_url;

            const thisDiv = document.createElement('a');

            thisDiv.setAttribute('class', 'user');
            thisDiv.setAttribute('href', 'https://gitlab.com/' + user.name);

            thisDiv.innerHTML = `
              <img src="${avatar}">
            `;

            $('.contributors').appendChild(thisDiv);
          });
      });
    });
}